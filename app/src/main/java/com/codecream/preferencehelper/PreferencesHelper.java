package com.codecream.preferencehelper;

import android.content.Context;
import android.content.SharedPreferences;

public class PreferencesHelper {
    private static PreferencesHelper instance;

    private PreferencesHelper(){

    }

    public static PreferencesHelper getInstance(){
        if(instance == null)
            instance = new PreferencesHelper();

        return instance;
    }

    public void putBoolean(Context context, String key, Boolean value){
        SharedPreferences prefs = context.getSharedPreferences("prefs", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    public void putInt(Context context, String key, int value){
        SharedPreferences prefs = context.getSharedPreferences("prefs", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt(key, value);
        editor.commit();
    }

    public Boolean getBool(Context context, String key, Boolean defValue){
        SharedPreferences prefs = context.getSharedPreferences("prefs", Context.MODE_PRIVATE);
        return prefs.getBoolean(key, defValue);
    }

    public int getInt(Context context, String key, int defValue){
        SharedPreferences prefs = context.getSharedPreferences("prefs", Context.MODE_PRIVATE);
        return prefs.getInt(key, defValue);
    }
}
